<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html lang="en">
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous" />

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<head>
  <meta charset="UTF-8"/>
  <style>

.rodona {
	-webkit-border-radius: 45px;
	-moz-border-radius: 45px;
	border-radius: 45px;
	border:7px solid #EBD4FF;
	background:rgba(211,227,134,0.9);
	-webkit-box-shadow: #B3B3B3 20px 20px 20px inset;
	-moz-box-shadow: #B3B3B3 20px 20px 20px inset;
	 box-shadow: #B3B3B3 20px 20px 20px inset;
}

.taula1 {
	width: 90% ; 
	height: 39px; 
	border: 1; 
	background: #b8cbfa; 
	border: 1px solid #000;
	font-family: 'Arial,Helvetica' ; 
	font-weight: bold;
	padding: 10px; 
	border-spacing: 10px;
    border-collapse: separate;

}  	
.lletra2{
	color : #ff0000;
	margin: 20px; 
}


.logo{
	width: 60px; 
	height: 60px; 
	}
TD {
	 border: 1px solid #000;
}

body {
	margin-left: 20px;
}
.fitxa { 
		margin-left: 20px;
		width: 90%; 
	    border-width: 1px;
	    border: 3px solid #000000;
	}

.row {
	    border: 3px solid #000000;
}	

.columna {
	    border: 3px solid #000000;
}

.color0 {  background-color: #ffffff ;  }
.color1 {  background-color: #b8cbfa ;  }
.color2 {  background-color: #b0c0f0 ;  }

.foto {
        border: 2px solid #cccccc;
}

.imatge {
	
	text-align: center;

  </style>

  </head>
<body>
<header>
	<p></p>
	<div class="row fitxa color1">
	  <a class="navbar-brand color1" href="#"> <span class="glyphicon glyphicon-search"> Cercador </span></a>
      <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-camera"> Imatges </span></a>				
	</div>
	</header>

<xsl:for-each select="minerals/mineral[@id='2']">

<p></p>
<div class="row fitxa color1" >
<div class="col-md-3 columna">  &#160; <span ><xsl:value-of select="nom" /></span></div>
<div class="col-md-3 columna">  <span><xsl:value-of select="nom"/></span></div>
<div class="col-md-3 columna">  <span><xsl:value-of select="composicio"/></span></div>
<div class="col-md-3 columna"> &#160;  <span>Grup:</span><span><xsl:value-of select="grup"/></span></div>
</div>
<p></p>
<p></p>
<div class="row fitxa color1">
<div class="col-md-6">
<div class="row"> <span class="lletra1">Pes espec&#237;fic: </span> <span class="lletra2"><xsl:value-of select="densitat"/></span></div>
<div class="row"><span class="lletra1">Duresa: </span><span class="lletra2"><xsl:value-of select="duressa"/></span></div>
<div class="row"><span class="lletra1">Exfoliaci&#243;: </span> <span class="lletra2"><xsl:text>---</xsl:text></span></div>
<div class="row"><span class="lletra1">Color: </span><span class="lletra2"><xsl:value-of select="color"/></span><span class="lletra1">Ratlla </span><span class="lletra2"> <xsl:text>---</xsl:text> </span></div>
<div class="row"> <span class="lletra1">Llu&#237;ssor: </span><span class="lletra2"><xsl:text>---</xsl:text></span></div>
<div class="row"><span class="lletra1">Cristal.litzaci&#243;: </span><span class="lletra2"><xsl:text>---</xsl:text></span></div>
<div class="row"><span class="lletra1">Aspecte: </span><span class="lletra2"><xsl:text>---</xsl:text></span></div>
</div>
<div class="col-md-6 color0 imatge">
<img src="{foto}" width="190" class="img-circle foto" /> 
<br /><br /><br /><SMALL> <xsl:text>---</xsl:text></SMALL>
</div>
</div>
<p></p>
<div class="container fitxa color1">
  <div class="row"><span class="lletra1">Carater&#237;stiques generals: </span> <span class="lletra2"><xsl:text>---</xsl:text></span></div>
<div class="row"> <span class="lletra1">Jaciments: </span> <span class="lletra2"><xsl:text>---</xsl:text></span></div>
<div class="row"> <span class="lletra1">Utilitats i aplicacions: </span> <span class="lletra2"><xsl:text>---</xsl:text></span></div>
<div class="row"><IMG class="logo" SRC='http://www.xtec.cat/~fmas/mine/logo.gif' /> &#160; &#160; &#160; <span class="lletra3">Fitxa original del Projecte </span></div>
</div>
<p></p>
</xsl:for-each>
</body>
</html>
  </xsl:template>
</xsl:stylesheet>




